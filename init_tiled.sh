#!/bin/bash
################################################################################
#
#
################################################################################


docker run -d --restart=always --net=host -e TILED_SINGLE_USER_API_KEY=secret \
    -v $PWD/config:/deploy/config:ro \
    registry.hzdr.de/hzb/bluesky/core/images/tiled:1.0.0 \
    tiled serve config /deploy/config/config.yml


