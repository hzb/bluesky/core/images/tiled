FROM python:3.10-slim-bullseye

ENV TZ=Europe/Berlin

# Install essential build tools and utilities.
RUN apt update -y && apt upgrade -y
RUN apt install -y --no-install-recommends tzdata 

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Update Ubuntu Software repository
RUN apt update
RUN apt-get -y update && apt-get -y install gcc
RUN pip install --upgrade pip
RUN pip install --upgrade databroker[all]==2.0.0b46

