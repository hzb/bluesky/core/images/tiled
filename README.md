### Example Tiled Deployment in Docker ###

Build the image with


`docker build -t registry.hzdr.de/hzb/bluesky/containers/tiled .`

Start the service with the following replace `secret` with your api_key. You will also need to change the config file path

```
docker run -d --net=host -e TILED_SINGLE_USER_API_KEY=secret -v /path/to/config:/deploy/config:ro registry.hzdr.de/hzb/bluesky/core/images/tiled:v1.0.2 tiled serve config /deploy/config/config.yml
```
This will start a tiled server on port 8000 using the api_key you define here.
The configuration file which is held in ./config/config.yml will be loaded

#### Connecting to Bluesky ####

You will need to install the required packages:

`python3 -m pip install --upgrade databroker[all]==2.0.0b46`

you can then connect to the db as follows (replace `host_ip` and `secret` as required)

```python
from tiled.client import from_uri
db= from_uri("http://host_ip:8000",api_key="secret")
```

To attach a run engine to write files use:

```python
from bluesky import RunEngine
RE = RunEngine({})

def post_document(name,doc):
    db.post_document(name,doc)
    
RE.subscribe(post_document)

```

Note that we no longer explicity use the databroker package, only tiled.



